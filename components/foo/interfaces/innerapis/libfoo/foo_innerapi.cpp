#include <iostream>
#include "bar_innerapi.h"

const char *FOO_GetInfo()
{
  std::cout << "BAR info from foo: " << BAR_GetInfo() << std::endl;
  return "foo";
}
