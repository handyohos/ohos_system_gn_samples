#include <iostream>
#include "baz_innerapi.h"

const char *BAR_GetInfo()
{
  std::cout << "BAZ info from bar: " << BAZ_GetInfo() << std::endl;
  return "bar";
}
