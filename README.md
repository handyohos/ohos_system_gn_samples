# ohos_system_gn_samples

#### 介绍
OpenHarmony基于gn及ninja构建系统镜像，本项目用于说明OpenHarmony为gn扩展的能力，并提供sample样例验证这些扩展能力。

#### 软件架构
部件（component）是OpenHarmony系统的基本架构单元，每个部件包含一系列编译目标，多个部件组合形成不同的产品。
架构上每个部件相互独立，部件间只能依赖InnerAPI，不能直接依赖其它部件内部模块。
gn原生不支持部件，OpenHarmony编译构建系统对gn进行了扩展，用于支持部件化架构。本项目通过定义最简单的部件模型，验证这些扩展能力。

本项目定义了三个部件：foo, bar, baz，验证以下能力：
1. 支持external_deps和public_external_deps跨部件innerapi依赖
   a）加载build_configs/parts_info/inner_kits_info.json，根据component:innerapi匹配依赖目标的绝对路径
   b）如果inner_kits_info.json中映射的绝对路径不存在时，映射到.hpm/components/{target_cpu}/{subsystem}/{component}/innerapis/{innerapi_name}二进制目标
2. 支持获取任意目标的component和subsystem：
   a) 加载build_configs/parts_info/parts_path_info.json，根据目录可以自动匹配component
   b）加载build_configs/parts_info/part_subsystem.json，根据component自动匹配subsystem
3. 支持判断目标是否为innerapi，如果是innerapi，则不允许为group或source_set
4. deps内容为绝对路径时，支持判断该路径是否为跨部件访问
5. include_dirs为绝对路径时，支持判断该路径是否为跨部件访问
6. import时，支持判断是否为跨部件访问


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

gn gen out
ninja -C out

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

